[![MasterHead](https://gitlab.com/widirahman62/widirahman62/-/raw/main/assets/WidisRoomPixelArt.png)](#)

<h1 align="center">Hi 👋, I'm Widi Rahman</h1>
<h4 align="center">An enthusiast exploring tech through<br>research and experimentation</h4>

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://www.linkedin.com/in/widirahman62/" target="blank"><img align="center" src="https://raw.githubusercontent.com/maurodesouza/profile-readme-generator/e49da3cfc5f88fce486acbaa230de6bcf8c9fda1/src/assets/icons/social/linkedin/default.svg" alt="widirahman62" height="30" width="40" /></a>
<a href="https://instagram.com/widirahman62" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="widirahman62" height="30" width="40" /></a>
<a href="https://github.com/widirahman62" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/github.svg" alt="widirahman62" height="30" width="40" /></a>
